﻿using Common.WolHelp;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WolController : ControllerBase
    {
        [HttpPost]
        public IActionResult Login([FromForm] string loginname, [FromForm] string loginpassword)
        {
            if (loginname == "cz_0004@163.com" && loginpassword == "chenzhe")
            {
                return Content(JsonConvert.SerializeObject(new { Status = true, Code = "200", Message = "OK" }));
            }
            else
            {
                return Content(JsonConvert.SerializeObject(new { Status = false, Code = "200", Message = "Fail" }));
            }
        }

        [HttpGet]
        public IActionResult SendWol()
        {
            var wol = new WakeOnLan();
            // 发送魔术数据包，唤醒远程计算机
            wol.WakeUp("2C-F0-5D-2E-C8-8F", "192.168.31.2", "255.255.255.0", 40000);

            // 判断远程计算机是否开启（由于防火墙等原因不一定有效，同时由于开机需要时间，通常等待数秒到一两分钟不等才能检测到远程计算机的状态）
            var computerAccessible = WakeOnLan.IsComputerAccessible("192.168.31.2");

            // 通过ARP协议尝试获取远程计算机的mac地址（通常局域网内有效）
            var mac = WakeOnLan.GetMACAddress("192.168.31.2");
            return Content(JsonConvert.SerializeObject(new { Status = true, Code = "200", Message = "OK", ComputerAccessible = computerAccessible }));
        }
    }
}